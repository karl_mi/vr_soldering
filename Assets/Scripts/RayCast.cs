﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class RayCast : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        LineRenderer ray = gameObject.AddComponent<LineRenderer>();
        ray.positionCount = 2;
        ray.startColor = Color.red;
        ray.endColor = Color.red;
        ray.widthMultiplier = 0.01f;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3[] pos = new Vector3[2];
        pos[0] = transform.position;
        pos[1] = transform.position + transform.forward;
        LineRenderer ray = GetComponent<LineRenderer>();
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 10f, 5))
        {
            pos[1] = hit.point;
            if (hit.collider.gameObject.ToString() == "Button (UnityEngine.GameObject)" && SteamVR_Input._default.inActions.InteractUI.GetLastStateDown(SteamVR_Input_Sources.RightHand))
            {
                Debug.Log("asdfjdsahnfhksda");
            }
        }
        Debug.DrawRay(transform.position, transform.forward * 5, Color.blue);
        ray.SetPositions(pos);
    }
}
