﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VREyeRaycaster : MonoBehaviour
{

    GameObject gui;
    // Use this for initialization
    void Start()
    {
        gui = GameObject.Find("Canvas");
    }

    // Update is called once per frame
    void Update()
    {
        gui.SetActive(false);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward * 2, out hit))
        {
            Debug.Log(hit.collider.gameObject.ToString());
            if (hit.collider.gameObject.ToString() == "Canvas_Dummy (UnityEngine.GameObject)")
            {
                gui.SetActive(true);
            }
        }
    }
}
